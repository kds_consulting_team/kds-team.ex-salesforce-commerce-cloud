ENDPOINT_DEF = {
    "orders": {
        "parent_table": {
            "orders": "orders.csv"
        },
        "child_table_definitions": {
            "orders.notes": "order_notes.csv",
            "orders.paymentInstruments": "order_payment_instruments.csv",
            "orders.productItems": "order_product_items.csv",
            "orders.shipments": "order_shipments.csv",
            "orders.shippingItems": "order_shipping_items.csv",
            "orders.shippingItems.priceAdjustments": "order_shipping_items_price_adjustments.csv",
            "orders.productItems.priceAdjustments": "order_payment_items_price_adjustments.csv",
            "orders.orderPriceAdjustments": "order_price_adjustments.csv",
            "orders.couponItems": "order_coupon_items.csv"
        },
        "table_primary_keys": {
            "orders": {
                "orders.orderNo": "orderNo"
            },
            "orders.notes": {
                "orders.orderNo": "orderNo",
                "orders.notes.subject": "note_subject",
                "orders.notes.creationDate": "note_creationDate"
            },
            "orders.paymentInstruments": {
                "orders.orderNo": "orderNo",
                "orders.paymentInstruments.paymentInstrumentId": "paymentInstrumentId"
            },
            "orders.productItems": {
                "orders.orderNo": "orderNo",
                "orders.productItems.itemId": "itemId",
                "orders.productItems.productId": "productId"
            },
            "orders.shipments": {
                "orders.orderNo": "orderNo",
                "orders.shipments.shipmentId": "shipmentId"
            },
            "orders.shippingItems": {
                "orders.orderNo": "orderNo",
                "orders.shippingItems.shipmentId": "shipmentId"
            },
            "orders.shippingItems.priceAdjustments": {
                "orders.orderNo": "orderNo",
                "orders.shippingItems.shipmentId": "shipmentId",
                "orders.shippingItems.priceAdjustments.priceAdjustmentId": "priceAdjustmentId",
                "orders.shippingItems.priceAdjustments.promotionId": "promotionId"
            },
            "orders.productItems.priceAdjustments": {
                "orders.orderNo": "orderNo",
                "orders.productItems.itemId": "itemId",
                "orders.productItems.productId": "productId",
                "orders.productItems.priceAdjustments.campaignId": "campaignId",
                "orders.productItems.priceAdjustments.priceAdjustmentId": "priceAdjustmentId",
                "orders.productItems.priceAdjustments.promotionId": "promotionId"
            },
            "orders.orderPriceAdjustments": {
                "orders.orderNo": "orderNo",
                "orders.orderPriceAdjustments.campaignId": "campaignId",
                "orders.orderPriceAdjustments.priceAdjustmentId": "priceAdjustmentId",
                "orders.orderPriceAdjustments.promotionId": "promotionId"
            },
            "orders.couponItems": {
                "orders.orderNo": "orderNo",
                "orders.couponItems.couponItemId": "couponItemId"
            }
        },
        "root_node": "data"
    },
    "promotions": {
        "parent_table": {
            "promotions": "promotions.csv"
        },
        "child_table_definitions": {
            "promotions.activeCampaignAssignments": "promotions_active_campaign_assignments.csv",
            "promotions.tags": "promotions_tags.csv"
        },
        "table_primary_keys": {
            "promotions": {
                "promotions.id": "id"
            },
            "promotions.activeCampaignAssignments": {
                "promotions.id": "promotionId",
                "promotions.campaignId": "campaignId"
            },
            "promotions.tags": {
                "promotions.id": "promotionId",
                "promotions.tagId": "tagId"
            }
        },
        "root_node": "hits"
    },
    "source_code_groups": {
        "parent_table": {
            "source_code_groups": "source_code_groups.csv"
        },
        "child_table_definitions": {
            "source_code_groups.specifications": "source_code_groups_specification.csv"
        },
        "table_primary_keys": {
            "source_code_groups": {
                "source_code_groups.id": "id"
            },
            "source_code_groups.specifications": {
                "source_code_groups.id": "groupId",
                "source_code_groups.specifications.expression": "expression"
            }
        },
        "root_node": "hits"
    },
    "catalogs": {
        "parent_table": {
            "catalogs": "catalogs.csv"
        },
        "child_table_definitions": {
            "catalogs.assignedSites": "catalogs_assignedSites.csv"
        },
        "table_primary_keys": {
            "catalogs": {
                "catalogs.id": "id"
            },
            "catalogs.assignedSites": {
                "catalogs.id": "catalog_id",
                "catalogs.assignedSites.id": "id"
            }
        },
        "root_node": "data"
    }
}
