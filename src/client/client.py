from keboola.http_client import HttpClient
from requests.exceptions import HTTPError

import base64

AUTH_URL = "https://account.demandware.com/dwsso/oauth2/access_token"
BASE_URL = "https://{SHORTCODE}.api.commercecloud.salesforce.com/"
VALID_ENDPOINTS = []


class SalesforceClientException(Exception):
    pass


class SalesforceClient(HttpClient):
    def __init__(self, client_id, client_secret, organization_id, short_code, encoded_auth_string=None):
        self.client_id = client_id
        self.client_secret = client_secret
        self.organization_id = organization_id
        self.tenant_id = organization_id.split("_", 2)[2]
        self.auth_token = None
        super().__init__(BASE_URL.replace("{SHORTCODE}", short_code), status_forcelist=(500, 502, 504, 409))
        self.login(encoded_auth_string=encoded_auth_string)

    def login(self, encoded_auth_string=None):
        params = {
            "grant_type": "client_credentials",
            "Content-Type": "application/x-www-form-urlencoded",
            "scope": f"SALESFORCE_COMMERCE_API:{self.tenant_id} "
                     f"sfcc.promotions sfcc.orders sfcc.source-codes sfcc.catalogs sfcc.customerlists"
        }

        base64_auth_string = self.base64_encode_auth(self.client_id, self.client_secret)

        header = {
            "Authorization": f"Basic {base64_auth_string}"
        }

        if encoded_auth_string:
            header = {
                "Authorization": f"Basic {encoded_auth_string}"
            }

        self.update_auth_header(header, overwrite=True)
        try:
            auth_response = self.post(AUTH_URL, data=params, headers=header)
        except HTTPError as http_err:
            raise SalesforceClientException("Failed to authenticate client. Make sure you username, password, "
                                            "and organization id are correct. More info from SF : "
                                            f"{http_err.response.text}") from http_err
        self.auth_token = auth_response.get("access_token")
        auth_header = {'Authorization': f'Bearer {self.auth_token}'}
        self.update_auth_header(auth_header, overwrite=True)

    @staticmethod
    def base64_encode_auth(client_id, client_secret):
        auth_string = f"{client_id}:{client_secret}"
        auth_string_bytes = auth_string.encode("ascii")

        base64_bytes = base64.b64encode(auth_string_bytes)
        return base64_bytes.decode("ascii")

    def get_orders(self, site_id, start_date, end_date, use_created_date):
        params = {
            "siteId": site_id,
            "limit": 200
        }

        if use_created_date:
            params["creationDateFrom"] = start_date
            params["creationDateTo"] = end_date
        else:
            params["lastModifiedDateFrom"] = start_date
            params["lastModifiedDateTo"] = end_date

        endpoint_path = f"/checkout/orders/v1/organizations/{self.organization_id}/orders"
        all_orders_fetched = False
        offset = 0
        while not all_orders_fetched:
            page = self._get_page(endpoint_path, params, offset)
            yield page
            if not page.get("data"):
                all_orders_fetched = True
            offset += 1

    def _get_page(self, endpoint, params, offset):
        if offset > 0:
            params["offset"] = offset
        response = self.get(endpoint_path=endpoint, params=params)
        return response

    def search_post_endpoint(self, site_id, endpoint_path):
        all_data_fetched = False
        params = {
            "siteId": site_id
        }
        body = {
            "limit": 150,
            "query": {
                "textQuery": {
                    "fields": ['id'],
                    "searchPhrase": "*"
                }
            },
            "offset": 1
        }
        while not all_data_fetched:
            response = self.post(endpoint_path=endpoint_path,
                                 params=params,
                                 json=body,
                                 headers={'Content-Type': 'application/json'})
            yield response
            if len(response.get("hits")) < 150:
                all_data_fetched = True
            body["offset"] += 1

    def get_promotions(self, site_id):
        endpoint_path = f"pricing/promotions/v1/organizations/{self.organization_id}/promotions"
        return self.search_post_endpoint(site_id, endpoint_path)

    def get_source_code_groups(self, site_id):
        endpoint_path = f"pricing/source-code-groups/v1/organizations/{self.organization_id}/source-code-groups"
        return self.search_post_endpoint(site_id, endpoint_path)

    def get_catalogs(self):
        params = {
            "limit": 50
        }
        endpoint_path = f"/product/catalogs/v1/organizations/{self.organization_id}/catalogs"
        all_orders_fetched = False
        offset = 1
        while not all_orders_fetched:
            page = self._get_page(endpoint_path, params, offset)
            if "data" in page:
                all_orders_fetched = True
            yield page
            offset += 1
