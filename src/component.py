import logging
import tempfile
import time
import warnings
from collections import OrderedDict

from keboola.component.base import ComponentBase
from keboola.component.exceptions import UserException
from keboola.csvwriter import ElasticDictWriter

from client import SalesforceClient, ENDPOINT_DEF, SalesforceClientException
from helpers import get_date_intervals, DateParseException
from json_parser import JSONParser, EndpointDefinition

warnings.filterwarnings(
    "ignore",
    message="The localize method is no longer necessary, as this time zone supports the fold attribute",
)

ENDPOINT_DEFINITION_PATH = "endpoint_definitions/endpoint_def.py"

KEY_CLIENT_ID = 'client_id'
KEY_CLIENT_SECRET = '#client_secret'
KEY_ORGANIZATION_ID = "organization_id"
KEY_TENANT_ID = "tenant_id"
KEY_SITE_ID = "site_id"
KEY_ENDPOINT = "endpoint"
KEY_BACKFILL_MODE = "backfill_mode"
KEY_SHORT_CODE = "short_code"

KEY_ENCODED_AUTH_STR = "#encoded_auth_string"

KEY_DATE_FROM = 'date_from'
KEY_DATE_TO = 'date_to'
KEY_USE_CREATED_DATE = 'use_created_date'

KEY_STATE_ENDPOINT_COLUMNS = "endpoint_columns"
KEY_STATE_FETCHED_INTERVALS = "fetched_intervals"
KEY_STATE_ENDPOINT = "endpoint"

REQUIRED_PARAMETERS = [KEY_CLIENT_ID, KEY_CLIENT_SECRET, KEY_ORGANIZATION_ID, KEY_TENANT_ID, KEY_SHORT_CODE]
REQUIRED_IMAGE_PARS = []

ENDPOINTS = ["promotions", "orders", "source_code_groups", "catalogs"]
ENDPOINTS_WITH_DATE_PARAMS = ["orders"]
ENDPOINTS_WITH_SITE_ID = ["promotions", "orders", "source_code_groups"]
DATE_INTERVAL = 5
COMPONENT_RUNTIME = 3600

# TODO: In general do typehints and docstrings straight away. It also helps to think about the design.
#  At least public methods. Most of the time I have no idea what happens here.


class Component(ComponentBase):
    def __init__(self):
        self.tables = {}
        self.new_state = {}
        self._writer_cache = dict()
        self.client = None
        self.fetched_intervals = []
        super().__init__()

    def run(self):
        self.validate_configuration_parameters(REQUIRED_PARAMETERS)
        self.validate_image_parameters(REQUIRED_IMAGE_PARS)
        params = self.configuration.parameters
        state = self.get_state_file()

        client_id = params.get(KEY_CLIENT_ID)
        client_secret = params.get(KEY_CLIENT_SECRET)
        organization_id = params.get(KEY_ORGANIZATION_ID)
        short_code = params.get(KEY_SHORT_CODE)
        site_id = params.get(KEY_SITE_ID)
        endpoint = params.get(KEY_ENDPOINT)
        backfill_mode = params.get(KEY_BACKFILL_MODE, False)
        use_created_date = params.get(KEY_USE_CREATED_DATE, False)

        encoded_auth_string = params.get(KEY_ENCODED_AUTH_STR)

        self.validate_config(endpoint, state)

        date_intervals = self.get_date_intervals(backfill_mode, state) if endpoint in ENDPOINTS_WITH_DATE_PARAMS else []

        try:
            self.client = SalesforceClient(client_id, client_secret, organization_id, short_code,
                                           encoded_auth_string)
        except SalesforceClientException as sfcc_exc:
            raise UserException(sfcc_exc) from sfcc_exc

        endpoint_columns = state.get(KEY_STATE_ENDPOINT_COLUMNS) if state.get(KEY_STATE_ENDPOINT_COLUMNS) else {}

        self.fetch_and_save_endpoint_data(endpoint, site_id, endpoint_columns, date_intervals, use_created_date)

        new_state = self.update_state(endpoint, backfill_mode)
        self.write_state_file(new_state)

        self._close_writers()
        self.write_table_manifests()

    def write_table_manifests(self):
        for table in self.tables:
            self.write_manifest(self.tables[table])

    def get_date_intervals(self, backfill_mode, state):
        params = self.configuration.parameters
        fetched_intervals = state.get("fetched_intervals")

        if backfill_mode:
            logging.info(f"Fetching with backfill mode. Already fetched data for intervals {fetched_intervals}")
        try:
            intervals_to_fetch, fetched_intervals = get_date_intervals(fetched_intervals,
                                                                       params.get(KEY_DATE_FROM),
                                                                       params.get(KEY_DATE_TO),
                                                                       DATE_INTERVAL,
                                                                       backfill_mode)
            self.fetched_intervals = fetched_intervals
            return intervals_to_fetch
        except DateParseException as date_err:
            raise UserException(date_err) from date_err

    @staticmethod
    def validate_config(endpoint, state):
        if endpoint not in ENDPOINTS:
            raise UserException(f"Selected endpoint '{endpoint}' is not in : {ENDPOINTS}")
        if state.get(KEY_STATE_ENDPOINT):
            if state.get(KEY_STATE_ENDPOINT) != endpoint:
                raise UserException("When running the configuration with a new endpoint, you must reset the state "
                                    f"of the component. {endpoint} dos not match "
                                    f"the last run endpoint : {state.get(KEY_STATE_ENDPOINT)}")

    def fetch_and_save_endpoint_data(self, endpoint_name, site_id, endpoint_columns, date_intervals, use_created_date):
        fieldnames = endpoint_columns if endpoint_columns else {}
        endpoint_definition = EndpointDefinition(ENDPOINT_DEF, endpoint_name)
        self.create_tables_from_endpoint_definition(endpoint_definition, endpoint_columns)

        endpoint_getter = self._get_endpoint_getter(self.client, endpoint_name)

        logging.info(f"Fetching data for endpoint : {endpoint_name}")

        json_parser = JSONParser(endpoint_definition)
        if endpoint_name in ENDPOINTS_WITH_DATE_PARAMS:
            self.fetch_and_process_endpoint_with_date(endpoint_name, endpoint_getter, site_id, json_parser, fieldnames,
                                                      date_intervals, use_created_date)
        elif endpoint_name in ENDPOINTS_WITH_SITE_ID:
            self.fetch_and_process_endpoint_with_site_id(endpoint_getter, site_id, json_parser, fieldnames)
        else:
            self.fetch_and_process_endpoint(endpoint_getter, json_parser, fieldnames)

    def fetch_and_process_endpoint(self, endpoint_getter, json_parser, fieldnames):
        start_time = time.time()
        for data in endpoint_getter():
            if (time.time() - start_time) > 900:
                self.client.login()
                logging.info("Re-authenticating the client")
                start_time = time.time()
            if data and json_parser.endpoint_definition.root_node in data:
                parsed_data = json_parser.parse_data(data)
                self.save_parsed_data(parsed_data, fieldnames)

    def fetch_and_process_endpoint_with_site_id(self, endpoint_getter, site_id, json_parser, fieldnames):
        start_time = time.time()
        for data in endpoint_getter(site_id):
            if (time.time() - start_time) > 900:
                self.client.login()
                logging.info("Re-authenticating the client")
                start_time = time.time()
            if data:
                parsed_data = json_parser.parse_data(data)
                self.save_parsed_data(parsed_data, fieldnames)

    def fetch_and_process_endpoint_with_date(self, endpoint_name, endpoint_getter, site_id, json_parser, fieldnames,
                                             date_intervals, use_created_date):
        token_start_time = time.time()
        extraction_start_time = time.time()

        for interval in date_intervals:
            logging.info(
                f"Fetching {endpoint_name} data for interval {interval['start_date']} to {interval['end_date']}")
            for data in endpoint_getter(site_id, interval["start_date"], interval["end_date"], use_created_date):
                # TODO: this should be handled by the client. Not here, it leads to duplicated code.
                # It can be a wrapper function/annotation on each client method.
                if (time.time() - token_start_time) > 900:
                    self.client.login()
                    logging.info("Re-authenticating the client")
                    token_start_time = time.time()
                if data:
                    parsed_data = json_parser.parse_data(data)
                    self.save_parsed_data(parsed_data, fieldnames)
            self.fetched_intervals.append(interval)

            # Stop fetching and save results after 75% of run time has passed
            if (time.time() - extraction_start_time) > COMPONENT_RUNTIME * 0.75:
                logging.warning("Maximum run time of the component has almost passed, "
                                "must save results and end fetching. If backfill mode is enabled, "
                                "then rerun the component and fetching will continue. If backfill mode is not enabled,"
                                " you should enable it to fetch the data in the specified date range")
                break

    def update_table_definitions(self):
        for wr in self._writer_cache.values():
            table_name = wr.table_name
            columns = wr.fieldnames
            self.update_table_columns(table_name, columns)

    def update_table_columns(self, table_name, columns):
        for table in self.tables:
            if table_name == table:
                self.tables[table].columns = columns

    @staticmethod
    def _get_endpoint_getter(client, endpoint_name):
        func_name = f"get_{endpoint_name}"
        try:
            return getattr(client, func_name)
        except AttributeError as attr_err:
            raise UserException(f"Function to fetch endpoint data of '{endpoint_name}' : '{func_name}' "
                                f"is not implemented") from attr_err

    def create_tables_from_endpoint_definition(self, endpoint_definition, endpoint_columns):
        for table in endpoint_definition.all_tables:
            table_name = endpoint_definition.all_tables[table]
            primary_keys = endpoint_definition.get_table_primary_key_names(table)
            table_columns = primary_keys.copy()
            object_name = table_name.replace(".csv", "")
            columns_from_state = endpoint_columns.get(object_name) if endpoint_columns.get(object_name) else []
            table_columns.extend(columns_from_state)
            table_columns = self.get_no_duplicate_list(table_columns)
            self.tables[table_name] = self.create_out_table_definition(table_name, primary_key=primary_keys,
                                                                       incremental=True, columns=table_columns)
            self._get_writer_from_cache(self.tables[table_name], self.tables[table_name].columns)

    @staticmethod
    def get_no_duplicate_list(list_):
        # list(set()) rearranges the list
        return list(OrderedDict.fromkeys(list_))

    def save_parsed_data(self, parsed_data, fieldnames):
        for data_name in parsed_data:
            table_fieldnames = fieldnames.get(data_name) if fieldnames.get(data_name) else []
            table_fieldnames.extend(self.tables[data_name].columns)
            table_fieldnames = self.get_no_duplicate_list(table_fieldnames)
            writer = self._get_writer_from_cache(self.tables[data_name], table_fieldnames)
            writer.writerows(parsed_data[data_name])

    def _get_writer_from_cache(self, out_table, fieldnames):
        if not self._writer_cache.get(out_table.name):
            # init writer if not in cache
            self._writer_cache[out_table.name] = ElasticDictWriter(out_table.full_path,
                                                                   fieldnames,
                                                                   temp_directory=tempfile.mkdtemp())

        return self._writer_cache[out_table.name]

    def _close_writers(self):
        for wr in self._writer_cache.values():
            wr.close()

    def update_state(self, endpoint, backfill_mode):
        new_state_column = {}
        for table in self.tables:
            table_name = self.tables[table].name.replace(".csv", "")
            new_state_column[table_name] = self.tables[table].columns
        new_state = {KEY_STATE_ENDPOINT_COLUMNS: new_state_column,
                     KEY_STATE_ENDPOINT: endpoint,
                     KEY_STATE_FETCHED_INTERVALS: []}
        if backfill_mode:
            new_state[KEY_STATE_FETCHED_INTERVALS] = self.fetched_intervals
        return new_state


if __name__ == "__main__":
    try:
        comp = Component()
        comp.execute_action()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
