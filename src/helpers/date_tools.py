import datetime
import keboola.utils.date as dutils
from typing import List, Dict


class DateParseException(Exception):
    pass


def get_date_intervals(fetched_intervals, date_from, date_to, date_interval, backfill_mode):
    fetched_intervals = fetched_intervals if fetched_intervals and backfill_mode else []
    fetched_intervals = merge_intervals(fetched_intervals)
    start_date, end_date = parse_datetime_interval(date_from, date_to)
    missing_dates = get_missing_dates(start_date, end_date, fetched_intervals)

    missing_date_intervals = get_intervals_from_dates(missing_dates)

    intervals_to_fetch = []

    for missing_date_interval in missing_date_intervals:
        intervals_to_fetch.extend(
            dutils.split_dates_to_chunks(missing_date_interval["start_date"], missing_date_interval["end_date"],
                                         intv=date_interval, strformat='%Y-%m-%d'))
    return intervals_to_fetch, fetched_intervals


def get_intervals_from_dates(list_of_dates):
    if not list_of_dates:
        return []
    intervals = []
    current_interval_start = None
    current_interval_end = None
    for date in list_of_dates:
        date = str_to_date(date)
        if not current_interval_start:
            current_interval_start = date
            current_interval_end = date
        elif (date - current_interval_end).days != 1:
            intervals.append(
                {"start_date": current_interval_start - datetime.timedelta(days=1),
                 "end_date": current_interval_end + datetime.timedelta(days=1)})
            current_interval_start = date
            current_interval_end = date
        else:
            current_interval_end = date
    intervals.append(
        {"start_date": current_interval_start - datetime.timedelta(days=1),
         "end_date": current_interval_end + datetime.timedelta(days=1)})

    return intervals


def get_missing_dates(start_date, end_date, fetched_intervals):
    fetched_dates = get_dates_from_intervals(fetched_intervals)
    dates_to_fetch = get_dates_from_interval(start_date, end_date)

    missing_dates = []
    for date in dates_to_fetch:
        if date not in fetched_dates:
            missing_dates.append(date)

    return missing_dates


def get_dates_from_intervals(intervals):
    dates = []
    for interval in intervals:
        dates.extend(get_dates_from_interval(interval["start_date"], interval["end_date"]))
    return dates


def get_dates_from_interval(start_date, end_date):
    dates = []
    start = datetime.datetime.strptime(start_date, '%Y-%m-%d')
    end = datetime.datetime.strptime(end_date, '%Y-%m-%d')
    date_generated = [start + datetime.timedelta(days=x) for x in range(0, (end - start).days + 1)]

    for date in date_generated:
        dates.append(date.strftime('%Y-%m-%d'))
    return dates


def str_to_date(date_str):
    return datetime.datetime.strptime(date_str, '%Y-%m-%d')


def parse_datetime_interval(date_from, date_to):
    try:
        start_date, end_date = dutils.parse_datetime_interval(date_from, date_to, strformat='%Y-%m-%d')
        return start_date, end_date
    except TypeError:
        raise DateParseException(f"Failed to parse dates, please check if {date_from} and {date_to} are valid inputs")


def merge_intervals(intervals: List[Dict]) -> List[Dict]:
    intervals.sort(key=lambda x: x["start_date"])
    merged = []
    for interval in intervals:
        # if the list of merged intervals is empty or if the current
        # interval does not overlap with the previous, simply append it.
        if not merged or merged[-1]["end_date"] < interval["start_date"]:
            merged.append(interval)
        else:
            # otherwise, there is overlap, so we merge the current and previous
            # intervals.
            merged[-1]["end_date"] = max(merged[-1]["end_date"], interval["end_date"])

    return merged
