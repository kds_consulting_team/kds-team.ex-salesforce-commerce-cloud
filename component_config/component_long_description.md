This component enables you to extract objects from Salesforce Commerce Cloud.

The following endpoints are currently supported:
* [Orders](https://developer.salesforce.com/docs/commerce/commerce-api/references/orders?meta=getOrders)
* [Promotions](https://developer.salesforce.com/docs/commerce/commerce-api/references/promotions?meta=getPromotion)
* [Catalogs](https://developer.salesforce.com/docs/commerce/commerce-api/references/catalogs?meta=getCatalogs)
* [Source Code Groups](https://developer.salesforce.com/docs/commerce/commerce-api/references/source-code-groups?meta=getSourceCodeGroup)