# Salesforce Commerce Cloud Extractor

Commerce Cloud is a multi-tenant, cloud-based commerce platform by Salesforce

This component enables you to extract objects from Salesforce Commerce Cloud


# Supported endpoints

- Orders - [Salesforce Documentation](https://developer.salesforce.com/docs/commerce/commerce-api/references/orders?meta=Summary)
- Promotions - [Salesforce Documentation](https://developer.salesforce.com/docs/commerce/commerce-api/references/promotions?meta=Summary)
- Catalogs - [Salesforce Documentation](https://developer.salesforce.com/docs/commerce/commerce-api/references/catalogs?meta=Summary)
- Source Code Groups - [Salesforce Documentation](https://developer.salesforce.com/docs/commerce/commerce-api/references/source-code-groups?meta=getSourceCodeGroup)


# Prerequisites

- In the Commerce Cloud Account Manager add an API Client. For details, refer to: [help.salesforce.com](https://help.salesforce.com/s/articleView?id=cc.b2c_account_manager_add_api_client_id.htm&type=5) 
- Enable API  Client ID. For details, refer to: [help.salesforce.com](https://help.salesforce.com/s/articleView?id=cc.b2c_account_manager_enable_disable_client_id.htm&type=5)

# Configuration


## Salesforce Commerce Cloud Authentication
 - Client ID (client_id) - [REQ] Client ID granted by the Salesforce Commerce Cloud Account Manager
 - Client Secret (#client_secret) - [REQ] Client Secret granted by the Salesforce Commerce Cloud Account Manager
 - Organization Id (organization_id) - [REQ] Get Organization Id from SFCC Business Manager by going to Administration > Site Development > Salesforce Commerce API Settings
 - Tenant Id (tenant_id) - [REQ] The tenant ID should be part of your organization ID f_ecom_{{tenant_id}}. Example: for organization ID f_ecom_zzzz_001, the tenant ID is zzzz_001.
 - Short code (short_code) - [REQ] Get Short code from SFCC Business Manager by going to Administration > Site Development > Salesforce Commerce API Settings

## Row Configuration
 - Endpoint (endpoint) - [REQ] description
 - Site ID (site_id) - [OPT] Site ID is the name of the site for which you want to access data, for example RefArch or SiteGenesis. To see a list of sites in Business Manager, go to Administration > Sites > Manage Sites
 - Last modified date from (date_from) - [OPT] Data that has been modified since this date will be fetched. Either a relative date can be set eg. 3 days ago, or an exact date eg. 2022-01-01
 - Last modified date to (date_to) - [OPT] Data that has been modified until this date will be fetched. Either a relative date can be set eg. 'now', or an exact date eg. 2022-01-01
 - Backfill (backfill_mode) - [OPT] If set to true, data will be fetched during multiple runs. 


# Sample Configuration

```json
{
    "parameters": {
        "client_id": "CLIENT_ID",
        "#client_secret": "SECRET_VALUE",
        "organization_id": "org_id",
        "tenant_id": "tenant_id",
        "short_code": "short_code",
        "site_id": "site_id",
        "date_from": "30 days ago",
        "date_to": "now",
        "endpoint": "orders",
        "backfill_mode": true
    },
    "action": "run"
}
```

Output
======

List of tables, foreign keys, schema.

Development
-----------

If required, change local data folder (the `CUSTOM_FOLDER` placeholder) path to your custom path in
the `docker-compose.yml` file:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    volumes:
      - ./:/code
      - ./CUSTOM_FOLDER:/data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Clone this repository, init the workspace and run the component with following command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose build
docker-compose run --rm dev
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Run the test suite and lint check using this command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose run --rm test
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Integration
===========

For information about deployment and integration with KBC, please refer to the
[deployment section of developers documentation](https://developers.keboola.com/extend/component/deployment/)